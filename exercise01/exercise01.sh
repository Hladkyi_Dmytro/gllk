#!/bin/bash

var_folders=""
var_depth="-maxdepth 1"
# var_regexp=
# var_del="-delete"
var_del=""
var_type="-type f"
var_print="-print"
vat_test=""
var_help="
Usage: ./exercise01.sh [FOLDER] [OPTION]\n
\n
Options:\n
-h, --help \t\t display this help and exit \n
-r, --recursive \t delete files recursively \n
-y, --yes \t\t delete files without question \n
-t, --test \t\t print filenames without delete \n
\n
wrote by Dmytro Hladkyi
"



for param in $@
do
	case "$param" in
		"-h"|"--help")		echo -e $var_help 
	  						exit;;

		"-r"|"--recursive")	var_depth="";;

		"-y"|"--yes") 		var_del="-delete";;

		"-t"|"--test") 		var_del=""
							var_test="yes";;

		*) 					if  [ -d $param ]; then
							var_folders+=" $param" 
							else
							echo "paremetr '$param' is not recognized"
							exit
							fi;;
	esac
done

if  [[ "$var_folders" == "" ]]
then
	var_folders="./"
fi

find $var_folders $var_depth -name "[_~\-]*" $var_type $var_print $var_del
find $var_folders $var_depth -name "*.tmp" $var_type $var_print $var_del


if [[ $var_del != "" || $var_test == "yes" ]]
then
	exit
fi

read -p "Do you want delete this files? (y or n) " answer

case $answer in
	y) var_del="-delete";;
	n) exit;;
esac

find $var_folders $var_depth -name "[_~\-]*" $var_type $var_print $var_del
find $var_folders $var_depth -name "*.tmp" $var_type $var_print $var_del