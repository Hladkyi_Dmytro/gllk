#!/bin/bash
current_dir=$(pwd)

main_menu1+=("switch_mode")
main_menu2+=("back")

file_menu_item_info="info"
file_menu_item_rename="rename"
file_menu_item_copy="copy"
file_menu_item_move="move"
file_menu_item_delete="delete"
file_menu=( "$file_menu_item_info $file_menu_item_rename $file_menu_item_copy $file_menu_item_move $file_menu_item_delete" )

menu_item_back="go_to_up_dir"
menu_item_settings="settings"
menu_item_exit="exit"
menu_item_copy="copy_file_here"
menu_item_move="move_file_here"
menu_item_delete_dir="delete_current_dir"
menu_item_cancel_operation="cancel_operation"
# menu_item_





# file_menu=$(ls)

mode="FOLDERS"
folders=$(ls )

choise=""





setting_show_hiden_files="show_hiden_files"
setting_do_not_show_hiden_folders="do_not_show_hiden_folders"

setting_hiden_files=$setting_show_hiden_files

ls_flag_hidden_files="-aA"

setting_view_show_files_and_folders="show_files_and_folders"
setting_view_show_only_folders="show_only_folders"
setting_view=$setting_view_show_files_and_folders
ls_flag_folders=""


setting_sort_do_not_sort="do_not_sort"
setting_sort=$setting_sort_do_not_sort
ls_flag_sort=""

settings_menu_back="back"

settings_menu=""


get_folders_list()
{
	folders=$(ls $ls_flag_hidden_files $ls_flag_folders $ls_flag_sort)
}

get_menu()
{
	case "$mode" in
		"FOLDERS" | "COPY" | "MOVE")
			# echo FOLDERS
			get_folders_list

			menu="$menu_item_back "
			if  [ $mode = "COPY" ]; then
				menu+="$menu_item_copy "
			elif [ $mode = "MOVE" ]; then
				menu+="$menu_item_move "
			fi

			menu+=$folders

			if  [ $mode = "FOLDERS" ]; then
				menu+=" $menu_item_delete_dir"
			elif [[ $mode = "COPY" || $mode = "MOVE" ]]; then
				menu+=" $menu_item_cancel_operation"	
			fi

			menu+=" $menu_item_settings"
		;;

		"FILE")
			# echo FILE
			menu=$file_menu
		;;

		"SETTINGS")
			 echo SETTINGS

			settings_menu="$setting_hiden_files $setting_view $setting_sort $settings_menu_back"
			menu=$settings_menu
			# menu+=" $menu_item_back"
		;;

		*) echo error_get_menu
		;;
	esac


	# if  [ ! $mode="SETTINGS" ]; then
		
	# fi

	menu+=" $menu_item_exit"

}



check_answer()
{
case $mode in
	"FOLDERS" | "COPY" | "MOVE")
		echo $choise
		if  [[ -d $choise ]]; then
			cd $choise
			current_dir=$(pwd)
			# mode="FOLDERS"
		elif [[ -f $choise ]]; then
			choice_filename=$choise
			mode="FILE" 
		elif [[ $choise == $menu_item_back ]]; then
			cd ..
			current_dir=$(pwd)
			
		elif [[ $choise == $menu_item_copy ]]; then
			new_filename=$choice_filename
			# read -p "print new filename " new_filename
			cp -v "$choice_dir/$choice_filename" "$current_dir/$new_filename"
			mode="FOLDERS"



		elif [[ $choise == $menu_item_move ]]; then
			new_filename=$choice_filename
			mv -v "$choice_dir/$choice_filename" "$current_dir/$new_filename"
			mode="FOLDERS"

		elif [[ $choise == $menu_item_cancel_operation ]]; then
			mode="FOLDERS"

		elif [[ $choise == $menu_item_delete_dir ]]; then
			cd ..
			rm -d $current_dir
			current_dir=$(pwd)
			mode="FOLDERS"

		elif [[ $choise == $menu_item_settings ]]; then
			mode="SETTINGS"

		elif [[ $choise == $menu_item_exit ]]; then
			exit 0
		else
			echo $choise
			echo error_check_answer_FOLDERS
		fi


	;;

	"FILE")
		case $choise in	
			$file_menu_item_info )
				stat $choice_filename
				mode="FOLDERS"
			;;
			$file_menu_item_rename)
				read -p "print new filename " new_filename
				mv -v $choice_filename   "$current_dir/$new_filename"
				mode="FOLDERS"
			;;

			$file_menu_item_copy) 
				choice_dir=$current_dir
				echo "choise directory"
				mode="COPY"
			;;

			$file_menu_item_move) 
				choice_dir=$current_dir
				echo "choise directory"
				mode="MOVE"
				;;

			$file_menu_item_delete)
				rm -rf $choice_filename
				mode="FOLDERS"
			;;


			$file_menu_item_back)
				mode="FOLDERS"
			;;

			$menu_item_exit)
				exit;;
			*)
				echo error_check_answer_FILE
			;;
		esac
	;;

	"SETTINGS")
		case $choise in	
			$setting_show_hiden_files )
				setting_hiden_files=$setting_do_not_show_hiden_folders
				ls_flag_hidden_files=""
			;;
			$setting_do_not_show_hiden_folders )
				setting_hiden_files=$setting_show_hiden_files
				ls_flag_hidden_files="-aA"
			;;
			$setting_view_show_files_and_folders )
				setting_view=$setting_view_show_only_folders
				ls_flag_folders="-d */"
			;;
			$setting_view_show_only_folders )
				setting_view=$setting_view_show_files_and_folders
				ls_flag_folders=""
			;;
			$setting_sort_do_not_sort )
				# ls_flag_sort=$setting_sort_do_not_sort
				echo "in progress..."
			;;
			# $ )
			
			# ;;
			# $ )
			
			# ;;
			# $ )
			
			# ;;
			# $ )
			
			# ;;
			$settings_menu_back)
				mode="FOLDERS"
			
			;;
			$menu_item_exit)
				exit;;
			*)
				echo error_check_answer_SETTINGS
			;;
		esac
	;;

	*) echo error_check_answer;
esac
}







while true; do

	get_menu

	select point in $menu
	do
		choise=$point
		break
	done


	check_answer


done